package net.fusionlord.cabinetsreloaded.packets;

import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import net.fusionlord.cabinetsreloaded.tileentity.CabinetTileEntity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class CabinetSyncPacket extends AbstractPacket
{
	int x, y, z;
	NBTTagCompound tagCompound;

	public CabinetSyncPacket()
	{
	}

	public CabinetSyncPacket(CabinetTileEntity cabinet)
	{
		x = cabinet.xCoord;
		y = cabinet.yCoord;
		z = cabinet.zCoord;
		tagCompound = new NBTTagCompound();
		cabinet.writeToNBT(tagCompound);
	}

	@Override
	public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer)
	{
		buffer.writeInt(x);
		buffer.writeInt(y);
		buffer.writeInt(z);
		ByteBufUtils.writeTag(buffer, tagCompound);
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer)
	{
		x = buffer.readInt();
		y = buffer.readInt();
		z = buffer.readInt();
		tagCompound = ByteBufUtils.readTag(buffer);
	}

	@Override
	public void handleClientSide(EntityPlayer player)
	{
		TileEntity te = player.worldObj.getTileEntity(x, y, z);
		if (te != null && te instanceof CabinetTileEntity)
		{
			((CabinetTileEntity) te).readFromNBT(tagCompound);
		}
	}

	@Override
	public void handleServerSide(EntityPlayer player)
	{
	}
}
