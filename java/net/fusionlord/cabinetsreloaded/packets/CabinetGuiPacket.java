package net.fusionlord.cabinetsreloaded.packets;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import net.fusionlord.cabinetsreloaded.tileentity.CabinetTileEntity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class CabinetGuiPacket extends AbstractPacket
{
	int x, y, z;
	boolean l, h;

	public CabinetGuiPacket()
	{
	}

	public CabinetGuiPacket(CabinetTileEntity cabinet)
	{
		x = cabinet.xCoord;
		y = cabinet.yCoord;
		z = cabinet.zCoord;

		l = cabinet.isLocked();
		h = cabinet.isHidden();
	}

	@Override
	public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer)
	{
		buffer.writeInt(x);
		buffer.writeInt(y);
		buffer.writeInt(z);

		buffer.writeBoolean(l);
		buffer.writeBoolean(h);
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer)
	{
		x = buffer.readInt();
		y = buffer.readInt();
		z = buffer.readInt();

		l = buffer.readBoolean();
		h = buffer.readBoolean();
	}

	@Override
	public void handleClientSide(EntityPlayer player)
	{
	}

	@Override
	public void handleServerSide(EntityPlayer player)
	{
		World world = player.worldObj;

		TileEntity te = world.getTileEntity(x, y, z);
		if (te instanceof CabinetTileEntity)
		{
			CabinetTileEntity cabinet = (CabinetTileEntity) te;

			cabinet.setLocked(l);
			cabinet.setHidden(h);
			cabinet.sync();
		}
	}
}
