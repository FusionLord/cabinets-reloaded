package net.fusionlord.cabinetsreloaded.packets;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import net.fusionlord.cabinetsreloaded.tileentity.CabinetTileEntity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;

/**
 * Author: FusionLord
 * Email: FusionLord@gmail.com
 */
public class CabinetNullifyOwnerPacket extends AbstractPacket
{
	int x, y, z;

	public CabinetNullifyOwnerPacket()
	{
	}

	public CabinetNullifyOwnerPacket(CabinetTileEntity cabinet)
	{
		this.x = cabinet.xCoord;
		this.y = cabinet.yCoord;
		this.z = cabinet.zCoord;
	}

	@Override
	public void encodeInto(ChannelHandlerContext ctx, ByteBuf buffer)
	{
		buffer.writeInt(x);
		buffer.writeInt(y);
		buffer.writeInt(z);
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf buffer)
	{
		this.x = buffer.readInt();
		this.y = buffer.readInt();
		this.z = buffer.readInt();
	}

	@Override
	public void handleClientSide(EntityPlayer player)
	{
	}

	@Override
	public void handleServerSide(EntityPlayer player)
	{
		TileEntity te = player.worldObj.getTileEntity(x, y, z);
		if (te instanceof CabinetTileEntity)
		{
			((CabinetTileEntity) te).setOwner(null);
		}
	}
}
