package net.fusionlord.cabinetsreloaded.block;

import net.fusionlord.cabinetsreloaded.CabinetsReloaded;
import net.fusionlord.cabinetsreloaded.Reference;
import net.fusionlord.cabinetsreloaded.item.CabinetItem;
import net.fusionlord.cabinetsreloaded.tileentity.CabinetTileEntity;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.BlockWorkbench;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.particle.EffectRenderer;
import net.minecraft.client.particle.EntityDiggingFX;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.Container;
import net.minecraft.item.Item;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.Explosion;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.ArrayList;
import java.util.List;

public class CabinetBlock extends BlockContainer
{
	public CabinetBlock()
	{
		super(Material.wood);
		setHardness(1.5f);
		setCreativeTab(CreativeTabs.tabDecorations);
		GameRegistry.registerBlock(this, CabinetItem.class, "Cabinet");
	}

	public static boolean getBlocked(World world, BlockPos pos, int facing)
	{
		int x = pos.getX();
		int y = pos.getY();
		int z = pos.getZ();
		boolean blocked = false;
		switch (facing)
		{
			case 0:
				blocked = !world.isAirBlock(new BlockPos(x, y, z - 1));
				break;
			case 1:
				blocked = !world.isAirBlock(new BlockPos(x + 1, y, z));
				break;
			case 2:
				blocked = !world.isAirBlock(new BlockPos(x, y, z + 1));
				break;
			case 3:
				blocked = !world.isAirBlock(new BlockPos(x - 1, y, z));
				break;
		}
		return blocked;
	}


	@Override
	public boolean removedByPlayer(World world, BlockPos pos, EntityPlayer player, boolean willHarvest)
	{
		TileEntity te = world.getTileEntity(pos);
		if (te != null && te instanceof CabinetTileEntity)
		{
			CabinetTileEntity cabinet = (CabinetTileEntity) te;
			if (cabinet.isOwner(player))
			{
				IBlockState state = world.getBlockState(pos);
				int meta = state.getBlock().getMetaFromState(state) << 12;
				boolean silky = false;
				ItemStack tool = player.getCurrentEquippedItem();
				if (tool != null && (tool.getItem() instanceof ItemAxe || tool.getItem() instanceof ItemPickaxe))
				{
					silky = EnchantmentHelper.getSilkTouchModifier(player);
				}
				if (player.capabilities.isCreativeMode || silky)
				{
					ItemStack stack = new ItemStack(this, 1, meta);
					boolean storeData = false;
					for (ItemStack itemStack : cabinet.getContents())
					{
						if (storeData)
						{
							break;
						}
						if (itemStack != null)
						{
							storeData = true;
						}
					}
					if (storeData || cabinet.getDisplayStack() != null)
					{
						NBTTagCompound cabinetTag = new NBTTagCompound();
						cabinet.writeExtraNBT(cabinetTag);
						stack.setTagCompound(new NBTTagCompound());
						stack.setTagInfo("silktouch", cabinetTag);
					}
					dropBlockAsItem(world, pos, world.getBlockState(pos), 0);
				}
				else
				{
					for (ItemStack stack : getDrops(world, pos, state, 0))
					{
						if (stack != null)
						{
							Block.spawnAsEntity(world, pos, stack);
						}
					}
				}
				world.setBlockToAir(pos);
				world.removeTileEntity(pos);
			}
		}
		return false;
	}

	@Override
	public ArrayList<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune)
	{
		ArrayList<ItemStack> ret = new ArrayList<>();

		TileEntity te = world.getTileEntity(pos);
		if (te instanceof CabinetTileEntity)
		{
			CabinetTileEntity cabinet = (CabinetTileEntity) te;
			ret.add(new ItemStack(Reference.cabinet, 1, state.getBlock().getMetaFromState(state) << 12));
			for (ItemStack stack : cabinet.getContents())
			{
				if (stack != null)
				{
					ret.add(stack);
				}
			}
			ret.add(cabinet.getDisplayStack());
		}
		return ret;
	}

	@Override
	public float getExplosionResistance(World world, BlockPos pos, Entity entity, Explosion explosion)
	{
		TileEntity tileEntity = world.getTileEntity(pos);
		if (tileEntity instanceof CabinetTileEntity)
		{
			CabinetTileEntity cabinet = (CabinetTileEntity) tileEntity;
			if (!cabinet.isLocked())
			{
				return Blocks.stone.getExplosionResistance(entity);
			}
		}
		return Blocks.bedrock.getExplosionResistance(entity);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean addHitEffects(World worldObj, MovingObjectPosition target, EffectRenderer effectRenderer)
	{
		Block block = worldObj.getBlockState(target.getBlockPos()).getBlock();

		if (worldObj.isAirBlock(target.getBlockPos()))
		{
			float f = 0.1F;

			int side = target.sideHit.getIndex();

			double d1 = target.getBlockPos().getY() + block.getBlockBoundsMinY() - (double) f * side == 0 ? 1 : -1;
			double d2 = target.getBlockPos().getZ() + block.getBlockBoundsMinZ() - (double) f * side == 2 ? 1 : -1;
			double d0 = target.getBlockPos().getX() + block.getBlockBoundsMinX() - (double) f * side == 4 ? 1 : -1;

			TileEntity te = worldObj.getTileEntity(target.getBlockPos());
			if (te instanceof CabinetTileEntity)
			{
				CabinetTileEntity cabinet = (CabinetTileEntity) te;
				block = cabinet.getDisplayStack() != null ? Block.getBlockFromItem(cabinet.getDisplayStack().getItem()) : Blocks.planks;
			}
			effectRenderer.addEffect(new EntityDiggingFX.Factory().getEntityFX(0,
											 worldObj,
											 d0,
											 d1,
											 d2,
											 0.0D,
											 0.0D,
											 0.0D,
											 Block.getIdFromBlock(block)
									 )
					/*.applyColourMultiplier(target.getBlockPos().getX(), target.getBlockPos().getY(), target.getBlockPos().getZ()).multiplyVelocity(0.2F)*/
					.multipleParticleScaleBy(0.6F));
		}
		return true;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean addDestroyEffects(World world, BlockPos pos, EffectRenderer effectRenderer)
	{
		int x = pos.getX();
		int y = pos.getY();
		int z = pos.getZ();
		Block block = world.getBlockState(pos).getBlock();
//		int displayMeta = 0;

		TileEntity te = world.getTileEntity(pos);
		if (te instanceof CabinetTileEntity)
		{
			CabinetTileEntity cabinet = (CabinetTileEntity) te;
			if (cabinet.getDisplayStack() != null)
			{
				block = Block.getBlockFromItem(cabinet.getDisplayStack().getItem());
//				displayMeta = cabinet.getDisplayStack().getItemDamage();
			}
			else
			{
				block = Blocks.planks;
//				displayMeta = 0;
			}
		}

		byte b0 = 4;

		for (int i1 = 0; i1 < b0; ++i1)
		{
			for (int j1 = 0; j1 < b0; ++j1)
			{
				for (int k1 = 0; k1 < b0; ++k1)
				{
					double d0 = (double) x + ((double) i1 + 0.5D) / (double) b0;
					double d1 = (double) y + ((double) j1 + 0.5D) / (double) b0;
					double d2 = (double) z + ((double) k1 + 0.5D) / (double) b0;

					effectRenderer.addEffect(new EntityDiggingFX.Factory().getEntityFX(0,
																					   world,
																					   d0,
																					   d1,
																					   d2,
																					   0.0D,
																					   0.0D,
																					   0.0D,
																					   Block.getIdFromBlock(block)
											 )
					/*.applyColourMultiplier(target.getBlockPos().getX(), target.getBlockPos().getY(), target.getBlockPos().getZ()).multiplyVelocity(0.2F)*/
													 .multipleParticleScaleBy(0.6F));
				}
			}
		}
		return true;
	}

	@Override
	public TileEntity createNewTileEntity(World world, int metadata)
	{
		return new CabinetTileEntity();
	}

	@Override
	public int getComparatorInputOverride(World world, BlockPos pos)
	{
		return Container.calcRedstoneFromInventory((CabinetTileEntity) world.getTileEntity(pos));
	}

	@Override
	public boolean hasComparatorInputOverride()
	{
		return true;
	}

	@Override
	public int getRenderType()
	{
		return Reference.CabinetRenderID;
	}

	@Override
	public boolean isSideSolid(IBlockAccess world, BlockPos pos, EnumFacing side)
	{
		EnumFacing face = EnumFacing.DOWN;
		TileEntity te = world.getTileEntity(pos);
		if (te != null && te instanceof CabinetTileEntity)
		{
			switch (((CabinetTileEntity) te).getFacing())
			{
				case 0:
					face = EnumFacing.NORTH;
					break;
				case 1:
					face = EnumFacing.EAST;
					break;
				case 2:
					face = EnumFacing.SOUTH;
					break;
				case 3:
					face = EnumFacing.WEST;
					break;
			}
		}
		return side != face;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public void onNeighborBlockChange(World world, BlockPos pos, IBlockState state, Block block)
	{
		int powered = world.isBlockIndirectlyGettingPowered(pos);
		CabinetTileEntity cabinet = (CabinetTileEntity) world.getTileEntity(pos);
		cabinet.setPowered(powered < 15);
		super.onNeighborBlockChange(world, pos, state, block);
	}

	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		int x = pos.getX();
		int y = pos.getY();
		int z = pos.getZ();
		TileEntity te = world.getTileEntity(pos);
		if (te == null || !(te instanceof CabinetTileEntity) || world.isRemote)
		{
			return true;
		}
		CabinetTileEntity cabinet = (CabinetTileEntity) te;
		if (cabinet.getDisplayStack() != null)
		{
			Block baseBlock = Block.getBlockFromItem(cabinet.getDisplayStack().getItem());
			if (baseBlock != null && baseBlock instanceof BlockWorkbench && side == EnumFacing.UP)
			{
				player.openGui(CabinetsReloaded.instance, 1, world, x, y, z);
				return true;
			}
		}

		if (cabinet.getOwner() == null)
		{
			cabinet.setOwner(player);
		}

		boolean blocked = getBlocked(world, pos, cabinet.getFacing());
		if (cabinet.isUseableByPlayer(player) && !blocked)
		{
			player.openGui(CabinetsReloaded.instance, 0, world, x, y, z);
			return true;
		}
		return true;
	}

	@Override
	public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase entity, ItemStack stack)
	{
		if (world.isRemote)
		{
			return;
		}
		int facing = MathHelper.floor_double(entity.rotationYaw * 4F / 360F + 0.5D) & 3;
		TileEntity te = world.getTileEntity(pos);
		if (te != null && te instanceof CabinetTileEntity && entity instanceof EntityPlayer)
		{
			CabinetTileEntity cabinet = (CabinetTileEntity) te;
			EntityPlayer player = (EntityPlayer) entity;
			if (!canPlace(world, pos, player, cabinet))
			{
				world.setBlockToAir(pos);
				world.removeTileEntity(pos);
				ItemStack newStack = stack.copy();
				newStack.stackSize = 1;
				player.inventory.addItemStackToInventory(newStack);
				return;
			}
			if (!stack.hasTagCompound())
			{
				cabinet.setOwner(player);
				cabinet.setFacing(facing);
			}
			else
			{
				cabinet.readExtraNBT(stack.getTagCompound().getCompoundTag("silktouch"));
				cabinet.setFacing(facing);
				cabinet.sync();
			}
		}
	}

	private boolean canPlace(World world, BlockPos pos, EntityPlayer player, CabinetTileEntity cabinet)
	{
		int x = pos.getX();
		int y = pos.getY();
		int z = pos.getZ();
		for (int x2 = -1; x2 < 2; x2++)
		{
			for (int y2 = -1; y2 < 2; y2++)
			{
				for (int z2 = -1; z2 < 2; z2++)
				{
					TileEntity tileEntity = world.getTileEntity(new BlockPos(x + x2, y + y2, z + z2));
					if (tileEntity != null && tileEntity instanceof CabinetTileEntity)
					{
						CabinetTileEntity cabinet2 = (CabinetTileEntity) tileEntity;
						if (cabinet != cabinet2)
						{
							if (!cabinet2.isOwner(player))
							{
								return false;
							}
						}
					}
				}
			}
		}
		return true;
	}

	@Override
	public int getLightValue(IBlockAccess world, BlockPos pos)
	{
		TileEntity te = world.getTileEntity(pos);
		if (te instanceof CabinetTileEntity)
		{
			CabinetTileEntity cabinet = (CabinetTileEntity) te;
			if (cabinet.getDisplayStack() != null)
			{
				Block baseBlock = Block.getBlockFromItem(cabinet.getDisplayStack().getItem());
				if (baseBlock != null)
				{
					return baseBlock.getLightValue();
				}
			}
		}
		return getLightValue();
	}

	@Override
	public float getBlockHardness(World world, BlockPos pos)
	{
		TileEntity te = world.getTileEntity(pos);
		if (te instanceof CabinetTileEntity)
		{
			CabinetTileEntity cabinet = (CabinetTileEntity) te;
			if (cabinet.getDisplayStack() != null)
			{
				Block baseBlock = Block.getBlockFromItem(cabinet.getDisplayStack().getItem());
				if (baseBlock != null)
				{
					return baseBlock.getBlockHardness(world, pos);
				}
			}
		}
		return blockHardness;
	}

//	@Override
//	public IIcon getIcon(IBlockAccess world, int x, int y, int z, int meta)
//	{
//		TileEntity te = world.getTileEntity(new BlockPos(x, y, z));
//		if (te instanceof CabinetTileEntity)
//		{
//			CabinetTileEntity cabinet = (CabinetTileEntity) te;
//			if (cabinet.getDisplayStack() != null)
//			{
//				Block baseBlock = Block.getBlockFromItem(cabinet.getDisplayStack().getItem());
//				if (baseBlock != null)
//				{
//					return baseBlock.getIcon(world, x, y, z, cabinet.getDisplayStack().getItemDamage());
//				}
//			}
//		}
//		return Blocks.planks.getIcon(world, x, y, z, 0);
//	}

	@Override
	public int damageDropped(IBlockState state)
	{
		return state.getBlock().getMetaFromState(state) << 12;
	}

	@Override
	@SideOnly(Side.CLIENT)
	@SuppressWarnings("all")
	public void getSubBlocks(Item item, CreativeTabs creativeTabs, List list)
	{
		list.add(new ItemStack(item, 1));
		list.add(new ItemStack(item, 1, 1));
		list.add(new ItemStack(item, 1, 2));
	}

	@Override
	public boolean shouldSideBeRendered(IBlockAccess worldIn, BlockPos pos, EnumFacing side)
	{
		return false;
	}
}