package net.fusionlord.cabinetsreloaded;

import net.fusionlord.cabinetsreloaded.block.CabinetBlock;
import net.fusionlord.cabinetsreloaded.config.Config;
import net.fusionlord.cabinetsreloaded.handlers.EventHandler;
import net.fusionlord.cabinetsreloaded.packets.PacketHandler;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class Reference
{
	public static final String MOD_ID = "cabinetsreloaded";
	public static final String MOD_VERSION = "@VERSION@";
	//public static int CabinetRenderID;
	public static PacketHandler packetHandler;
	public static CabinetBlock cabinet;
	public static Config config;

	public static boolean showItemsTileEntity = true;
	public static boolean showItemsItem = true;
	public static int cabinetYield = 8;
	public static int oldCabinetYield = cabinetYield;

	public static IRecipe currentCabinetRecipe;

	public static void init()
	{
		config.load();
		//CabinetRenderID = RenderingRegistry.getNextAvailableRenderId();
		cabinet = new CabinetBlock();
		packetHandler = new PacketHandler();
		packetHandler.initialise();
		FMLCommonHandler.instance().bus().register(new EventHandler());
	}

	public static void addRecipes()
	{
		addCabinetRecipe();
		GameRegistry.addShapelessRecipe(new ItemStack(cabinet, 1, 1), new ItemStack(cabinet, 1, 0));
		GameRegistry.addShapelessRecipe(new ItemStack(cabinet, 1, 2), new ItemStack(cabinet, 1, 1));
		GameRegistry.addShapelessRecipe(new ItemStack(cabinet, 1), new ItemStack(cabinet, 1, 2));
	}

	public static ResourceLocation getResource(String resource)
	{
		return new ResourceLocation(MOD_ID, resource);
	}

	public static void addCabinetRecipe()
	{
		currentCabinetRecipe = GameRegistry.addShapedRecipe(new ItemStack(cabinet, cabinetYield), "ppp", "pgp", "ppp", 'p',
				new ItemStack(Blocks.planks, 0, 0), 'g', new ItemStack(Blocks.glass));
		oldCabinetYield = cabinetYield;
	}
}
