package net.fusionlord.cabinetsreloaded.client.renderer;

import net.fusionlord.cabinetsreloaded.Reference;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.obj.WavefrontObject;

/**
 * Author: FusionLord
 * Email: FusionLord@gmail.com
 */
public class RenderingReference
{
	public static WavefrontObject model;
	public static WavefrontObject doorModel;
	public static ResourceLocation door;
	public static ResourceLocation doubleDoor;

	public static void init()
	{
		model = (WavefrontObject) AdvancedModelLoader.loadModel(Reference.getResource("models/cabinet.obj"));
		doorModel = (WavefrontObject) AdvancedModelLoader.loadModel(Reference.getResource("models/cabinetdoors.obj"));
		door = Reference.getResource("textures/models/doors.png");
		doubleDoor = Reference.getResource("textures/models/ddoor.png");
	}
}
