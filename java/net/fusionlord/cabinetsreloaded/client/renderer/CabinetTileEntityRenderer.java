package net.fusionlord.cabinetsreloaded.client.renderer;

import net.fusionlord.cabinetsreloaded.Reference;
import net.fusionlord.cabinetsreloaded.tileentity.CabinetTileEntity;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import org.lwjgl.opengl.GL11;

public class CabinetTileEntityRenderer extends TileEntitySpecialRenderer
{

	private final EntityItem dummyEntityItem;
	private RenderItem customRenderItem;

	public CabinetTileEntityRenderer()
	{
		super();
		customRenderItem = new RenderItem()
		{
			public boolean shouldBob()
			{
				return false;
			}

			public boolean shouldSpreadItems()
			{
				return false;
			}

			@Override
			public byte getMiniItemCount(ItemStack stack, byte original)
			{
				return 1;
			}

			@Override
			public byte getMiniBlockCount(ItemStack stack, byte original)
			{
				return 1;
			}
		};
		RenderItem.renderInFrame = true;
		dummyEntityItem = new EntityItem(null);
		dummyEntityItem.hoverStart = 0.0f;
		dummyEntityItem.motionX = 0.0f;
		dummyEntityItem.motionY = 0.0f;
		dummyEntityItem.motionZ = 0.0f;
		customRenderItem.setRenderManager(RenderManager.instance);
	}

	public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float f)
	{
		GL11.glPushMatrix();
		GL11.glTranslatef((float) x + .5f, (float) y, (float) z + .5f);
		CabinetTileEntity te = (CabinetTileEntity) tileEntity;
		renderCabinet(te, tileEntity.getWorldObj(), te.xCoord, te.yCoord, te.zCoord);
		GL11.glPopMatrix();
	}

	public void renderCabinet(CabinetTileEntity cabinet, World world, int x, int y, int z)
	{
		Tessellator tessellator = Tessellator.instance;
		float lightValue = Reference.cabinet.getLightValue(world, x, y, z);
		tessellator.setBrightness((int) lightValue);

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

		int facing = cabinet.getFacing();
		int face = 0;
		switch (facing)
		{
			case 0:
				face = 180;
				break;
			case 1:
				face = 90;
				break;
			case 2:
				face = 0;
				break;
			case 3:
				face = 270;
				break;
		}

		ItemStack displayStack = cabinet.getDisplayStack();
		if (displayStack == null)
		{
			displayStack = new ItemStack(Blocks.planks, 1, 0);
		}

		bindTexture(TextureMap.locationBlocksTexture);
		for (int side = 0; side < 6; side++)
		{
			Util.parts part = Util.parts.values()[side];
			for (int i = 1; i <= part.count; i++)
			{
				if ((side == ForgeDirection.DOWN.ordinal() && i == 2))
				{
					GL11.glRotatef(180, 0F, 1F, 0F);
				}
				if (!((side == ForgeDirection.DOWN.ordinal() && i == 2) || (side == ForgeDirection.UP.ordinal()
						&& i == 1)))
				{
					GL11.glRotatef(face, 0F, 1F, 0F);
				}
				IIcon icon = Block.getBlockFromItem(displayStack.getItem()).getIcon(side, displayStack.getItemDamage());
				if (icon == null)
				{
					icon = Blocks.planks.getIcon(side, 0);
				}
				Util.renderPartWithIcon(RenderingReference.model, part.name().concat(Integer.toString(i)), icon,
						tessellator, -1);
				if (!((side == ForgeDirection.DOWN.ordinal() && i == 2) || (side == ForgeDirection.UP.ordinal()
						&& i == 1)))
				{
					GL11.glRotatef(face, 0F, -1F, 0F);
				}
				if ((side == ForgeDirection.DOWN.ordinal() && i == 2))
				{
					GL11.glRotatef(180, 0F, -1F, 0F);
				}
			}
		}

		GL11.glRotatef(face - 90, 0F, 1F, 0F);

		float a = 90 * cabinet.getDoorAngle();
		float m = 0.0625F;
		switch (cabinet.getBlockMetadata())
		{
			case 0:
				bindTexture(RenderingReference.door);
				GL11.glTranslatef(m * 7f, m * 1f, m * 8f - ((a / 90) * m));
				GL11.glRotatef(a, 0f, -1f, 0f);
				RenderingReference.doorModel.renderPart("LeftDoor");
				GL11.glRotatef(a, 0f, 1f, 0f);
				GL11.glTranslatef(-(m * 7f), -(m * 1f), -(m * 8f) + ((a / 90) * m));
				break;

			case 1:
				bindTexture(RenderingReference.door);
				GL11.glTranslatef(m * 7f, m * 1f, -(m * 8f) + ((a / 90) * m));
				GL11.glRotatef(a, 0f, 1f, 0f);
				RenderingReference.doorModel.renderPart("RightDoor");
				GL11.glRotatef(a, 0f, -1f, 0f);
				GL11.glTranslatef(-(m * 7f), -(m * 1f), m * 8f - ((a / 90) * m));
				break;

			case 2:
				bindTexture(RenderingReference.doubleDoor);

				GL11.glTranslatef(m * 7f, m * 1f, m * 8f - ((a / 90) * m));
				GL11.glRotatef(a, 0f, -1f, 0f);
				RenderingReference.doorModel.renderPart("DLeftDoor");
				GL11.glRotatef(a, 0f, 1f, 0f);
				GL11.glTranslatef(-(m * 7f), -(m * 1f), -(m * 8f) + ((a / 90) * m));
				GL11.glTranslatef(m * 7f, m * 1f, -(m * 8f) + ((a / 90) * m));
				GL11.glRotatef(a, 0f, 1f, 0f);
				RenderingReference.doorModel.renderPart("DRightDoor");
				GL11.glRotatef(a, 0f, -1f, 0f);
				GL11.glTranslatef(-(m * 7f), -(m * 1f), m * 8f - ((a / 90) * m));
				break;
		}

		GL11.glRotatef(90, 0F, 1F, 0F);

		if (!cabinet.isHidden() && Reference.showItemsTileEntity)
		{
			float scale = .5f;
			float i, j, k;
			ItemStack[] contents = cabinet.getContents();
			GL11.glPushMatrix();
			GL11.glTranslatef(0f, 1f, 0f);
			GL11.glScalef(scale, scale, scale);
			for (int c = 0; c < contents.length; c++)
			{
				if (contents[c] == null)
				{
					continue;
				}
				ItemStack is = contents[c];
				i = ((c % 3) * .5f) - .8f * scale - .1f;
				j = ((-c / 3) * .5f) - .53f - (.1f * (c / 3));
				k = ((c / 3) * .5f) - .56f * scale;
				GL11.glTranslatef(i, j, k);
				dummyEntityItem.setEntityItemStack(is);
				for (int e = 0; e < c / 3 + 1; e++)
				{
					GL11.glTranslatef(0, 0, -.4f * e);
					if (contents[c].stackSize >= e + 1)
					{
						customRenderItem.doRender(dummyEntityItem, 0, 0, 0, 0, 0);
					}
					GL11.glTranslatef(0, 0, .4f * e);
				}
				GL11.glTranslatef(-i, -j, -k);
			}
			GL11.glPopMatrix();
		}
		GL11.glDisable(GL11.GL_BLEND);
	}
}