package net.fusionlord.cabinetsreloaded.client.renderer;

import net.minecraft.client.renderer.Tessellator;
import net.minecraftforge.client.model.b3d.B3DModel;
import org.lwjgl.opengl.GL11;

public class Util
{
	public static void renderPartWithIcon(WavefrontObject model, String name, IIcon icon, Tessellator tes, int color)
	{
		for (GroupObject go : model.groupObjects)
		{
			if (go.name.equals(name))
			{
				tes.startDrawing(GL11.GL_TRIANGLES);
				if (color != -1)
				{
					tes.setColorOpaque_I(color);
				}
				for (B3DModel.Face f : go.faces)
				{
					Vertex n = f.faceNormal;
					tes.setNormal(n.x, n.y, n.z);
					for (int i = 0; i < f.vertices.length; i++)
					{
						Vertex v = f.vertices[i];
						TextureCoordinate t = f.textureCoordinates[i];
						tes.addVertexWithUV(v.x, v.y, v.z,
								icon.getInterpolatedU(t.u * 16),
								icon.getInterpolatedV(t.v * 16));
					}
				}
				tes.draw();
			}
		}
	}

	enum parts
	{
		Bottom(4),
		Top(4),
		Left(2),
		Front(9),
		Back(1),
		Right(2);

		int count;

		private parts(int count)
		{
			this.count = count;
		}
	}
}