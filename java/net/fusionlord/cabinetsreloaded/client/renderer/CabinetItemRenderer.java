package net.fusionlord.cabinetsreloaded.client.renderer;

import net.fusionlord.cabinetsreloaded.Reference;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.client.IItemRenderer;
import org.lwjgl.opengl.GL11;

public class CabinetItemRenderer implements IItemRenderer
{
	private final EntityItem dummyEntityItem;
	private RenderItem customRenderItem;

	public CabinetItemRenderer()
	{
		customRenderItem = new RenderItem();
		RenderItem.renderInFrame = true;
		dummyEntityItem = new EntityItem(null);
		dummyEntityItem.hoverStart = 0.0f;
		dummyEntityItem.motionX = 0.0f;
		dummyEntityItem.motionY = 0.0f;
		dummyEntityItem.motionZ = 0.0f;
		customRenderItem.setRenderManager(RenderManager.instance);
	}

	@Override
	public boolean handleRenderType(ItemStack itemStack, ItemRenderType type)
	{
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper)
	{
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack itemStack, Object... data)
	{
		ItemStack[] contents = new ItemStack[0];
		ItemStack displayStack = new ItemStack(Blocks.planks);
		boolean hidden = true;
		TextureManager tm = Minecraft.getMinecraft().renderEngine;

		GL11.glPushMatrix();

		GL11.glTranslatef(0f, -.5f, 0f);
		GL11.glRotatef(90, 0f, 1f, 0f);

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		if (itemStack.hasTagCompound())
		{
			NBTTagCompound tag = itemStack.getTagCompound().getCompoundTag("silktouch");
			ItemStack potentialDisplayStack = ItemStack.loadItemStackFromNBT(tag.getCompoundTag("displayStack"));
			if (potentialDisplayStack != null && potentialDisplayStack.getItem() instanceof ItemBlock)
			{
				displayStack = potentialDisplayStack;
			}
			hidden = tag.getBoolean("hidden");
			contents = new ItemStack[9];
			NBTTagCompound inv = tag.getCompoundTag("inv");
			for (int i = 0; i < contents.length; i++)
			{
				contents[i] = ItemStack.loadItemStackFromNBT(inv.getCompoundTag("slot:".concat(String.valueOf(i))));
			}
		}

		tm.bindTexture(TextureMap.locationBlocksTexture);
		for (int side = 0; side < 6; side++)
		{
			Util.parts part = Util.parts.values()[side];
			for (int i = 1; i <= part.count; i++)
			{
				IIcon icon = Block.getBlockFromItem(displayStack.getItem()).getIcon(side, displayStack.getItemDamage());
				if (icon == null)
				{
					continue;
				}
				Util.renderPartWithIcon(RenderingReference.model, part.name().concat(Integer.toString(i)),
						icon, Tessellator.instance, -1
				);
			}
		}

		switch (itemStack.getItemDamage())
		{
			case 0:
				tm.bindTexture(RenderingReference.door);
				RenderingReference.model.renderPart("DoorLeft");
				break;
			case 1:
				tm.bindTexture(RenderingReference.door);
				RenderingReference.model.renderPart("DoorRight");
				break;
			case 2:
				tm.bindTexture(RenderingReference.doubleDoor);
				RenderingReference.model.renderPart("DDoorLeft");
				RenderingReference.model.renderPart("DDoorRight");
				break;
		}
		GL11.glDisable(GL11.GL_BLEND);

		if (!hidden && Reference.showItemsItem)
		{
			float scale = .5f;
			float i, j, k;
			GL11.glPushMatrix();
			GL11.glTranslatef(0f, 1f, 0f);
			GL11.glScalef(scale, scale, scale);
			for (int c = 0; c < contents.length; c++)
			{
				if (contents[c] == null)
				{
					continue;
				}
				ItemStack is = contents[c];
				i = ((c % 3) * .5f) - .8f * scale - .1f;
				j = ((-c / 3) * .5f) - .53f - (.1f * (c / 3));
				k = ((c / 3) * .5f) - .56f * scale;
				GL11.glTranslatef(i, j, k);
				dummyEntityItem.setEntityItemStack(is);
				for (int e = 0; e < c / 3 + 1; e++)
				{
					GL11.glTranslatef(0, 0, -.4f * e);
					if (contents[c].stackSize >= e + 1)
					{
						customRenderItem.renderItem(dummyEntityItem, 0, 0, 0, 0, 0);
					}
					GL11.glTranslatef(0, 0, .4f * e);
				}
				GL11.glTranslatef(-i, -j, -k);
			}
			GL11.glPopMatrix();
		}
		GL11.glPopMatrix();
	}
}
