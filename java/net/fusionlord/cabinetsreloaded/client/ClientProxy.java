package net.fusionlord.cabinetsreloaded.client;

import net.fusionlord.cabinetsreloaded.CommonProxy;
import net.fusionlord.cabinetsreloaded.Reference;
import net.fusionlord.cabinetsreloaded.client.renderer.CabinetItemRenderer;
import net.fusionlord.cabinetsreloaded.client.renderer.CabinetTileEntityRenderer;
import net.fusionlord.cabinetsreloaded.client.renderer.RenderingReference;
import net.fusionlord.cabinetsreloaded.tileentity.CabinetTileEntity;
import net.minecraft.item.Item;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.fml.client.registry.ClientRegistry;

public class ClientProxy extends CommonProxy
{
	@Override
	public void registerRenderers()
	{
		ClientRegistry.bindTileEntitySpecialRenderer(CabinetTileEntity.class, new CabinetTileEntityRenderer());
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(Reference.cabinet), new CabinetItemRenderer());
		RenderingReference.init();
	}
}
