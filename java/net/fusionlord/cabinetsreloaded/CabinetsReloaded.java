package net.fusionlord.cabinetsreloaded;

import net.fusionlord.cabinetsreloaded.config.Config;
import net.fusionlord.cabinetsreloaded.handlers.GuiHandler;
import net.fusionlord.cabinetsreloaded.tileentity.CabinetTileEntity;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod(modid = Reference.MOD_ID, name = "Cabinets Reloaded", version = Reference.MOD_VERSION, guiFactory = "net.fusionlord.cabinetsreloaded.config.GuiFactory")
public class CabinetsReloaded
{

	@Mod.Instance(Reference.MOD_ID)
	public static CabinetsReloaded instance;

	@SidedProxy(clientSide = "net.fusionlord.cabinetsreloaded.client.ClientProxy",
			serverSide = "net.fusionlord.cabinetsreloaded.CommonProxy")
	public static CommonProxy proxy;

	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		Reference.config = new Config(event.getSuggestedConfigurationFile());
		Reference.init();
	}

	@EventHandler
	public void load(FMLInitializationEvent event)
	{
		proxy.registerRenderers();
		GameRegistry.registerTileEntity(CabinetTileEntity.class, "fusionlord's_cabinet");
		NetworkRegistry.INSTANCE.registerGuiHandler(this, new GuiHandler());
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event)
	{
		Reference.packetHandler.postInitialise();
		Reference.addRecipes();
	}
}